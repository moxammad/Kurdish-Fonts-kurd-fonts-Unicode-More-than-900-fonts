Kurd Fonts unicode 991 Fonts

Here are 991 kurdish fonts. All are unicode.

These fonts have been taken from https://www.kurdfonts.com/ and all collected in a zip file and separately to help kurdish researchers use them without wasting times on downloading each font separately.
The mentioned website above is the source for the fonts and has the type, shape, class of each fonts separately.
